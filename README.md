Install traefik (do not use the docker version) and run it with the default configuration : https://traefik.io/#easy-to-install

Have docker daemon running

Clone the project

cd wamanager
bundle install
rails s

Go to http://localhost:8080 to see traefik dashboard
Create a domain to have a docker running
