class Domain < ApplicationRecord

  after_create :setup

  def setup
    create_mysql_user
    create_env_file
    create_docker_file
    run_docker
  end

  def create_mysql_user
    require 'mysql2'

    db_host = "localhost"
    db_user = "root"
    db_pass = "toor"

    client = Mysql2::Client.new(:host => db_host, :username => db_user, :password => db_pass)
    client.query("GRANT ALL PRIVILEGES ON *.* TO '#{name}'@'localhost' IDENTIFIED BY '#{name}'")
    client.close

    client2 = Mysql2::Client.new(:host => db_host, :username => name, :password => name)
    client2.query("CREATE DATABASE #{name}")
    client2.close
  end

  def create_env_file
    File.open("./tmp/#{name}.env", 'w') do |f|
      f << "WA_DB_ENGINE=MYSQL\n"
      f << "WA_DB_HOSTNAME=#{name}\n"
      f << "WA_DB_PORT=3306\n"
      f << "WA_DB_USERNAME=#{name}\n"
      f << "WA_DB_PASSWORD=#{name}"
    end
  end

  def create_docker_file
    File.open("./tmp/#{name}.docker-compose.yml", 'w') do |f|
      f << {
        "version" =>'3',
        "services" => {
          "test" => {
            "image" => 'tutum/hello-world',
            "labels" => {
              "traefik.frontend.rule" => "Host:#{name}.wamanager.com",
              "traefik.backend" => name}
          }
        }
      }.to_yaml
    end
  end

  def run_docker
    `docker-compose -f ./tmp/#{name}.docker-compose.yml -p #{name} up -d`
  end

end
